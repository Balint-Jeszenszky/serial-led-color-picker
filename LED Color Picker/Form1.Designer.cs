﻿namespace LED_Color_Picker
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.trackBlue = new System.Windows.Forms.TrackBar();
            this.trackGreen = new System.Windows.Forms.TrackBar();
            this.trackRed = new System.Windows.Forms.TrackBar();
            this.colorpanel = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.redVal = new System.Windows.Forms.Label();
            this.blueVal = new System.Windows.Forms.Label();
            this.greenVal = new System.Windows.Forms.Label();
            this.colorLabel = new System.Windows.Forms.Label();
            this.comport = new System.Windows.Forms.TextBox();
            this.com = new System.Windows.Forms.Label();
            this.rainbow = new System.Windows.Forms.Button();
            this.off = new System.Windows.Forms.Button();
            this.dark = new System.Windows.Forms.Button();
            this.available = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.trackBlue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackGreen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackRed)).BeginInit();
            this.SuspendLayout();
            // 
            // trackBlue
            // 
            this.trackBlue.Location = new System.Drawing.Point(100, 250);
            this.trackBlue.Maximum = 255;
            this.trackBlue.Name = "trackBlue";
            this.trackBlue.Size = new System.Drawing.Size(300, 45);
            this.trackBlue.TabIndex = 0;
            this.trackBlue.TickFrequency = 5;
            this.trackBlue.Scroll += new System.EventHandler(this.trackBlue_Scroll);
            // 
            // trackGreen
            // 
            this.trackGreen.Location = new System.Drawing.Point(100, 200);
            this.trackGreen.Maximum = 255;
            this.trackGreen.Name = "trackGreen";
            this.trackGreen.Size = new System.Drawing.Size(300, 45);
            this.trackGreen.TabIndex = 1;
            this.trackGreen.TickFrequency = 5;
            this.trackGreen.Scroll += new System.EventHandler(this.trackGreen_Scroll);
            // 
            // trackRed
            // 
            this.trackRed.BackColor = System.Drawing.SystemColors.Control;
            this.trackRed.Location = new System.Drawing.Point(100, 150);
            this.trackRed.Maximum = 255;
            this.trackRed.Name = "trackRed";
            this.trackRed.Size = new System.Drawing.Size(300, 45);
            this.trackRed.TabIndex = 2;
            this.trackRed.TickFrequency = 5;
            this.trackRed.Scroll += new System.EventHandler(this.trackRed_Scroll);
            // 
            // colorpanel
            // 
            this.colorpanel.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.colorpanel.Location = new System.Drawing.Point(150, 25);
            this.colorpanel.Name = "colorpanel";
            this.colorpanel.Size = new System.Drawing.Size(200, 100);
            this.colorpanel.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(59, 250);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Blue";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(59, 150);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Red";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(59, 200);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Green";
            // 
            // redVal
            // 
            this.redVal.AutoSize = true;
            this.redVal.Location = new System.Drawing.Point(406, 150);
            this.redVal.Name = "redVal";
            this.redVal.Size = new System.Drawing.Size(13, 13);
            this.redVal.TabIndex = 7;
            this.redVal.Text = "0";
            // 
            // blueVal
            // 
            this.blueVal.AutoSize = true;
            this.blueVal.Location = new System.Drawing.Point(406, 250);
            this.blueVal.Name = "blueVal";
            this.blueVal.Size = new System.Drawing.Size(13, 13);
            this.blueVal.TabIndex = 8;
            this.blueVal.Text = "0";
            // 
            // greenVal
            // 
            this.greenVal.AutoSize = true;
            this.greenVal.Location = new System.Drawing.Point(406, 200);
            this.greenVal.Name = "greenVal";
            this.greenVal.Size = new System.Drawing.Size(13, 13);
            this.greenVal.TabIndex = 9;
            this.greenVal.Text = "0";
            // 
            // colorLabel
            // 
            this.colorLabel.AutoSize = true;
            this.colorLabel.Location = new System.Drawing.Point(147, 9);
            this.colorLabel.Name = "colorLabel";
            this.colorLabel.Size = new System.Drawing.Size(88, 13);
            this.colorLabel.TabIndex = 10;
            this.colorLabel.Text = "Color: rgb(0, 0, 0)";
            // 
            // comport
            // 
            this.comport.Location = new System.Drawing.Point(65, 98);
            this.comport.Name = "comport";
            this.comport.Size = new System.Drawing.Size(20, 20);
            this.comport.TabIndex = 11;
            this.comport.TextChanged += new System.EventHandler(this.comport_TextChanged);
            // 
            // com
            // 
            this.com.AutoSize = true;
            this.com.Location = new System.Drawing.Point(28, 101);
            this.com.Name = "com";
            this.com.Size = new System.Drawing.Size(31, 13);
            this.com.TabIndex = 12;
            this.com.Text = "COM";
            // 
            // rainbow
            // 
            this.rainbow.Location = new System.Drawing.Point(380, 25);
            this.rainbow.Name = "rainbow";
            this.rainbow.Size = new System.Drawing.Size(75, 25);
            this.rainbow.TabIndex = 13;
            this.rainbow.Text = "Rainbow";
            this.rainbow.UseVisualStyleBackColor = true;
            this.rainbow.Click += new System.EventHandler(this.rainbow_Click);
            // 
            // off
            // 
            this.off.Location = new System.Drawing.Point(380, 60);
            this.off.Name = "off";
            this.off.Size = new System.Drawing.Size(75, 25);
            this.off.TabIndex = 14;
            this.off.Text = "Off";
            this.off.UseVisualStyleBackColor = true;
            this.off.Click += new System.EventHandler(this.off_Click);
            // 
            // dark
            // 
            this.dark.Location = new System.Drawing.Point(380, 95);
            this.dark.Name = "dark";
            this.dark.Size = new System.Drawing.Size(75, 25);
            this.dark.TabIndex = 15;
            this.dark.Text = "Dark";
            this.dark.UseVisualStyleBackColor = true;
            this.dark.Click += new System.EventHandler(this.dark_Click);
            // 
            // available
            // 
            this.available.AutoSize = true;
            this.available.Location = new System.Drawing.Point(28, 25);
            this.available.Name = "available";
            this.available.Size = new System.Drawing.Size(53, 13);
            this.available.TabIndex = 16;
            this.available.Text = "Available:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 311);
            this.Controls.Add(this.available);
            this.Controls.Add(this.dark);
            this.Controls.Add(this.off);
            this.Controls.Add(this.rainbow);
            this.Controls.Add(this.com);
            this.Controls.Add(this.comport);
            this.Controls.Add(this.colorLabel);
            this.Controls.Add(this.greenVal);
            this.Controls.Add(this.blueVal);
            this.Controls.Add(this.redVal);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.colorpanel);
            this.Controls.Add(this.trackRed);
            this.Controls.Add(this.trackGreen);
            this.Controls.Add(this.trackBlue);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LED Color Picker";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.trackBlue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackGreen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackRed)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TrackBar trackBlue;
        private System.Windows.Forms.TrackBar trackGreen;
        private System.Windows.Forms.TrackBar trackRed;
        private System.Windows.Forms.Panel colorpanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label redVal;
        private System.Windows.Forms.Label blueVal;
        private System.Windows.Forms.Label greenVal;
        private System.Windows.Forms.Label colorLabel;
        private System.Windows.Forms.TextBox comport;
        private System.Windows.Forms.Label com;
        private System.Windows.Forms.Button rainbow;
        private System.Windows.Forms.Button off;
        private System.Windows.Forms.Button dark;
        private System.Windows.Forms.Label available;
    }
}

